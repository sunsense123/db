package com.example.donghyunna.myapplication;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Jeonghyun Lee on 2015. 9. 20..
 */
public class DBManager_table_manager extends SQLiteOpenHelper {
    ArrayList<String> stateList = new ArrayList<String>();
    public Context mcontext;
    public DBManager_table_manager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        mcontext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
      //      SQLiteDatabase db = getWritableDatabase();
            sqLiteDatabase.execSQL("CREATE TABLE Table_Management(tableID INTEGER PRIMARY KEY ,startTime TEXT ,No_of_people TEXT ,Grouped_tables TEXT ,State TEXT);");

            Log.e("sahdjkasdnsajk", "아오 시발");
            Log.e("sahdjkasdnsajk", "다넣었다고 시발");
        //    db.close();
        }
        catch (Exception e){
            Log.e(e.toString(), "dB가 만들어 지지 않는다 으허");
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Table_Management;");
        onCreate(sqLiteDatabase);
    }
    public void insert(String _query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(_query);
        db.close();
    }

    public void update(String _query) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL(_query);
            db.close();
        }
        catch(Exception e){
            Log.e(e.toString(), e.toString());
        }
    }

    public void delete(String _query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(_query);
        db.close();
    }


    public long returnstartTime(int table_id) {
        SQLiteDatabase db = getReadableDatabase();
        long starttime;

        Cursor cursor = db.rawQuery("SELECT startTime FROM Table_Management", null);
        for(int i =1; i != table_id ; i++){
            cursor.moveToNext();
        }
        starttime = cursor.getInt(0);


        return starttime;
    }

    public String return_No_Of_People(int table_id) {
        SQLiteDatabase db = getReadableDatabase();
        String str = "nothing";

        Cursor cursor = db.rawQuery("SELECT No_of_people FROM Table_Management", null);
        while(cursor.moveToNext()) {
            str = cursor.getString(0);
        }

        return str;
    }
    public String return_Grouped_tables(int table_id) {
        SQLiteDatabase db = getReadableDatabase();
        String str = "nothing";

        Cursor cursor = db.rawQuery("select Grouped_tables FROM Table_Management", null);
        while(cursor.moveToNext()) {
            str = cursor.getString(0);
        }

        return str;
    }
    public ArrayList<String> returnState() {
        SQLiteDatabase db = getReadableDatabase();
        try {

            Cursor cursor = db.rawQuery("SELECT State FROM Table_Management", null);
            while(cursor.moveToNext()){
                stateList.add(cursor.getString(0));
            }


        }
        catch (Exception e){
            Log.e("ㄴ이ㅓㅏㄹ님러나이", "sakdhsajkdhkasjhfdksaj");
            Log.e("returnstate", e.toString());
        }

        return stateList;
    }

}
