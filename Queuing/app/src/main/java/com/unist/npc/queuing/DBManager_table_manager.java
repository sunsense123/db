package com.unist.npc.queuing;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Jeonghyun Lee on 2015. 9. 20..
 */
public class DBManager_table_manager extends SQLiteOpenHelper {
    ArrayList<String> stateList = new ArrayList<String>();
    public Context mcontext;
    public DBManager_table_manager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        mcontext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL("CREATE TABLE Table_Management(tableID INTEGER PRIMARY KEY ,startTime TEXT ,No_of_people TEXT ,Grouped_tables TEXT ,State TEXT);");

            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(1, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(2, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(3, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(4, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(5, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(6, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(7, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(8, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(9, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(10, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(11, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(12, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(13, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(14, 0, 0, 0, 0);");
            sqLiteDatabase.execSQL("INSERT INTO Table_Management VALUES(15, 0, 0, 0, 0);");
        }
        catch (Exception e){
            Log.e("onCreate of Table_DB", e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Table_Management;");
        onCreate(sqLiteDatabase);
    }
    public void insert(String _query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(_query);
        db.close();
    }

    public void update(String _query) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL(_query);
            db.close();
        }
        catch(Exception e){
            Log.e("update of Table_DB",e.toString());
        }
    }

    public void delete(String _query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(_query);
        db.close();
    }


    public long returnstartTime(int table_id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT startTime FROM Table_Management where tableID = " + String.valueOf(table_id), null);
        cursor.moveToNext();


        return Long.valueOf(cursor.getString(0));
    }

    public String return_No_Of_People(int table_id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT No_of_people FROM Table_Management where tableID = " + String.valueOf(table_id), null);
        cursor.moveToNext();


        return String.valueOf(cursor.getString(0));
    }
    public String return_Grouped_tables(int table_id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select Grouped_tables FROM Table_Management where tableID = " + String.valueOf(table_id), null);
        cursor.moveToNext();


        return String.valueOf(cursor.getString(0));
    }
    public ArrayList<String> returnState() {
        SQLiteDatabase db = getReadableDatabase();
        try {

            Cursor cursor = db.rawQuery("SELECT State FROM Table_Management", null);
            while(cursor.moveToNext()){
                stateList.add(cursor.getString(0));
            }

        }
        catch (Exception e){
            Log.e("returnstate",e.toString());
        }

        return stateList;
    }

}
